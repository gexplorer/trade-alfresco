#!/bin/bash
ALFRESCO_ID="alfresco"

PWD=`pwd`

REPOSITORY_DIST="${PWD}/repository/amp"
SHARE_DIST="${PWD}/share/amp"

REPOSITORY_AMPS_PATH="/var/lib/tomcat7/amps"
SHARE_AMPS_PATH="/var/lib/tomcat7/amps_share"

APPLY_AMPS_SCRIPT="/usr/share/tomcat7/bin/apply_amps.sh"

echo "# Stopping alfresco"
docker exec ${ALFRESCO_ID} /etc/init.d/tomcat7 stop

echo "# Applying AMPS"
docker run \
--rm \
--volumes-from ${ALFRESCO_ID} \
--user tomcat7 \
-v ${REPOSITORY_DIST}:${REPOSITORY_AMPS_PATH} \
-v ${SHARE_DIST}:${SHARE_AMPS_PATH} \
gexplorer/alfresco \
${APPLY_AMPS_SCRIPT}

echo "# Starting alfresco"
docker exec ${ALFRESCO_ID} /etc/init.d/tomcat7 start

#java -jar /usr/share/tomcat7/bin/alfresco-mmt.jar install /var/lib/tomcat7/amps/repository.amp /var/lib/tomcat7/webapps/alfresco.war -verbose

#java -jar /usr/share/tomcat7/bin/alfresco-mmt.jar list /var/lib/tomcat7/webapps/alfresco.war
#java -jar /usr/share/tomcat7/bin/alfresco-mmt.jar list /var/lib/tomcat7/webapps/share.war