#!/bin/bash
ALFRESCO_ID="alfresco"

PWD=`pwd`

REPOSITORY_AMP="${PWD}/repository/amp/repository.amp"
SHARE_AMP="${PWD}/share/amp/share.amp"

USER="florette"
HOST="alfresco.develop.florette.es"
PORT=22

REPOSITORY_AMPS_PATH="/home/florette/alfresco/repository/amp"
SHARE_AMPS_PATH="/home/florette/alfresco/share/amp"

echo "# Uploading repository"
scp -P $PORT $REPOSITORY_AMP $USER@$HOST:$REPOSITORY_AMPS_PATH

echo "# Uploading share"
scp -P $PORT $SHARE_AMP $USER@$HOST:$SHARE_AMPS_PATH
